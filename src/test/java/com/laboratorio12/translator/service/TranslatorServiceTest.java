package com.laboratorio12.translator.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TranslatorServiceTest {
    @Autowired
    public TranslatorService translatorService;

    @Test
    public void copyFile() throws FileNotFoundException {
        String originPathFileName = "src/test/resources/Original.txt";
        String targetPathFileName = "src/test/resources/CopyOriginal.txt";

        try {
            translatorService.copyFile(originPathFileName, targetPathFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String originFileContent = null;

        try {
            originFileContent = translatorService.getStringFromInputStream(new FileInputStream(originPathFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String targetFileContent = null;
        try {
            targetFileContent = translatorService.getStringFromInputStream(new FileInputStream(targetPathFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Comprobar que se ha copiado correctamente el archivo. ambos contenidos deben ser iguales para que el test sea TRUE
        assertEquals(originFileContent, targetFileContent);
    }

    @Test
    public void copyAndRevertContentFile () {
        String originPathFileName = "src/test/resources/Original.txt";
        String targetPathFileName = "src/test/resources/estrofasEnOrdenInverso.txt";

        // Copiar y revertir el contenido
        try {
            translatorService.copyAndInvertTheContentsOfFile(originPathFileName, targetPathFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String targetFileContent = null;
        try {
            targetFileContent = translatorService.getStringFromInputStream(new FileInputStream(targetPathFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertNotNull(targetFileContent);
    }

    @Test
    public void countStrophes() {
        String originPathFileName = "src/test/resources/estrofasEnOrdenInverso.txt";
        String targetPathFileName = "src/test/resources/Statistics.txt";
        int count = 0;
        // Contar estrofas
        try {
            count = translatorService.countStrophesOfFile(originPathFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Copiar el resultado a otro archivo
        String nexContent= "Cantidad de Estrofas = " + Integer.toString(count);
        try {
            translatorService.copyContentStringOfFile(targetPathFileName, nexContent);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String targetFileContent = null;
        try {
            targetFileContent = translatorService.getStringFromInputStream(new FileInputStream(targetPathFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertNotNull(targetFileContent);

    }

    @Test
    public void replaceSpacesInContentFile () {
        String originPathFileName = "src/test/resources/estrofasEnOrdenInverso.txt";
        String targetPathFileName = "src/test/resources/remplazarApostrofesConEspacios.txt";

        try {
            translatorService.replaceTextAContentFile(" ", "'", originPathFileName, targetPathFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String targetFileContent = null;
        try {
            targetFileContent = translatorService.getStringFromInputStream(new FileInputStream(targetPathFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertNotNull(targetFileContent);
    }

    @Test
    public void searchForMostRepeatedWord() {
        // palabra I = 36
        String pathFileName = "src/test/resources/remplazarApostrofesConEspacios.txt";
        String targetFileName = "src/test/resources/Statistics.txt";

        String contentFile = "";
        String[] wordMoreRepeated = {};

        try {
            wordMoreRepeated = translatorService.searchForMostRepeatedWord(pathFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            contentFile = translatorService.getContentOfFile(targetFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        contentFile = contentFile + "\n" + "Palabra mas repetida = " + wordMoreRepeated[0].toUpperCase() + "\n"
                     + "Cantidad de veces que se repite = " + wordMoreRepeated[1];

        try {
            translatorService.copyContentStringOfFile(targetFileName, contentFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String targetFileContent = null;
        try {
            targetFileContent = translatorService.getStringFromInputStream(new FileInputStream(targetFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertNotNull(targetFileContent);

    }

    @Test
    public void replaceTextInContentFile() {

        String originPathFileName = "src/test/resources/remplazarApostrofesConEspacios.txt";
        String targetPathFileName = "src/test/resources/finaloutput.txt";

        try {
            translatorService.replaceTextAContentFile("you", "I", originPathFileName, targetPathFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String targetFileContent = null;
        try {
            targetFileContent = translatorService.getStringFromInputStream(new FileInputStream(targetPathFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertNotNull(targetFileContent);
    }

}