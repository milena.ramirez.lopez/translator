package com.laboratorio12.translator;

import com.laboratorio12.translator.service.TranslatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class TranslatorApplication implements CommandLineRunner {

    private static Logger LOG = LoggerFactory.getLogger(TranslatorApplication.class);

    @Autowired
    public TranslatorService translatorService;

    public static void main(String[] args) {
        SpringApplication.run(TranslatorApplication.class, args);
    }

    @Override
    public void run(String... args) {
        LOG.info("EXECUTING TRANSLATOR");

        System.out.println("\n");
        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println("EXECUTING TRANSLATOR");
        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println("\n");
        //Declaramos paths de los archivos a crear
        String originPathFile = "src/main/resources/Original.txt";
        String estrofasInvertidasPath = "src/main/resources/estrofasEnOrdenInverso.txt";
        String estadisticasPath = "src/main/resources/Statistics.txt";
        String remplazarConEspaciosPath = "src/main/resources/remplazarApostrofesConEspacios.txt";
        String finaloutputPath = "src/main/resources/finaloutput.txt";

        // Paso 1: Copiar archivo original en orden inverso de estrofas
        System.out.println("Paso 1: Copiar archivo original.txt en orden inverso al archivo estrofasEnOrdenInverso.txt");
        try {
            translatorService.copyAndInvertTheContentsOfFile(originPathFile, estrofasInvertidasPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Paso 2: Contar las estrofas y escribir el resultado en el archivo Statistics.txt
        System.out.println("Paso 2: Contar las estrofas y escribir el resultado en el archivo Statistics.txt");
        int count = 0;
        // Contar estrofas
        try {
            count = translatorService.countStrophesOfFile(estrofasInvertidasPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Copiar el resultado a otro archivo
        String nexContent= "Cantidad de Estrofas = " + Integer.toString(count);
        try {
            translatorService.copyContentStringOfFile(estadisticasPath, nexContent);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Paso 3: Reemplazar apostrofes con espacios,  escribir el resultado en el archivo remplazarApostrofesConEspacios.txt
        System.out.println("Paso 3: Reemplazar apostrofes con espacios,  escribir el resultado en el archivo remplazarApostrofesConEspacios.txt");
        try {
            translatorService.replaceTextAContentFile(" ", "'", estrofasInvertidasPath, remplazarConEspaciosPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Paso 4: Buscar la palabra que mas se repite,  escribir el resultado en el archivo Statistics.txt
        System.out.println("Paso 4: Buscar la palabra que mas se repite,  escribir el resultado en el archivo Statistics.txt");
        String contentFile = "";
        String[] wordMoreRepeated = {};

        try {
            wordMoreRepeated = translatorService.searchForMostRepeatedWord(remplazarConEspaciosPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            contentFile = translatorService.getContentOfFile(estadisticasPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        contentFile = contentFile + "\n" + "Palabra mas repetida = " + wordMoreRepeated[0].toUpperCase() + "\n" + "Cantidad de veces que se repite = " + wordMoreRepeated[1];

        try {
            translatorService.copyContentStringOfFile(estadisticasPath, contentFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Paso 5: Reemplazar la palabra que mas se repite con la palabra you,  escribir el resultado en el archivo finaloutput.txt
        System.out.println("Paso 5: Reemplazar la palabra que mas se repite con la palabra you,  escribir el resultado en el archivo finaloutput.txt");
        try {
            translatorService.replaceTextAContentFile("you", "I", remplazarConEspaciosPath, finaloutputPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
