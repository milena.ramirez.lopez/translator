package com.laboratorio12.translator.service;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import org.apache.commons.io.IOUtils;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class TranslatorService {

    // Copiar un archivo
    public void copyFile(String originPathFile, String targetPathFile) throws IOException {

        File outputFile = new File(targetPathFile);
        InputStream originFile = new FileInputStream(originPathFile);
        OutputStream targetFile = new FileOutputStream(outputFile);
        // copiar archivo
        StreamUtils.copy(originFile, targetFile);
    }

    // Copiar e invertir contenido
    public void copyAndInvertTheContentsOfFile(String originPathFile, String targetPathFile) throws IOException {
        // Obtener el contenido en un String
        String content = getContentOfFile(originPathFile);
        // Obtener el contenido de la cadena por estrofas
        String[] strophes = content.split("\\n\\n");

        // Recorrer el arreglo y obtener una cadena
        String revert = "";
        for (int i = strophes.length - 1 ; i >= 0 ; i--) {
            if (i!=0) {
                revert += strophes[i] + "\n\n";
            } else {
                revert += strophes[i];
            }
        }

        // Copiar el contenido de la cadena a un archivo
        copyContentStringOfFile(targetPathFile, revert);
    }

    // Contar estrofas de un archivo
    public int countStrophesOfFile(String originPathFile) throws IOException {
        String content = getContentOfFile(originPathFile);
        String[] strophes = content.split("\\n\\n");
        int countStrophes = strophes.length;

        return countStrophes;
    }

    // Remplazar una palabra, caracter en el contenido de una archivo
    public void replaceTextAContentFile (String textReplace, String oldText, String pathFile, String targetPathFile) throws IOException {
        String originalContent = getContentOfFile(pathFile);
        String newContent = originalContent.replace(oldText, textReplace);

        copyContentStringOfFile(targetPathFile, newContent);
    }

    // Buscar la pabraba mas repetida
    public String[] searchForMostRepeatedWord(String pathfile) throws IOException {

        String contentFile = getContentOfFile(pathfile);

        // obtener un arreglo de las palabras eliminando espacios, comas, saltos de linea
        String[] words = contentFile.split("[^A-Za-z0-9]");
        HashMap<String, Integer> map = new HashMap <String, Integer> ();

        // Llenar un Hashmap con los datos del arreglo de palabras y contando la cantidad de veces que se repite
        for (String word : words) {
            if (word.equals("")) continue;
            word = word.toLowerCase();
            if (!map.containsKey(word))
                map.put(word, 1);
            else
                map.put(word, map.get(word).intValue() + 1);
        }
        // Obtener el valor mas alto
        int maxValueInMap = (Collections.max(map.values()));

        // Obtener el key del valor mas alto
        String keyWithHighestVal = "";
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() == maxValueInMap) {
                keyWithHighestVal = entry.getKey();
            }
        }

        String[] response = {keyWithHighestVal, Integer.toString(maxValueInMap)};
        return response;
    }

    // Remplazar una palabra, caracter en el contenido de una cadena
    public String replaceTextAStringContent (String textReplace, String oldText, String pathFile) throws IOException {
        String originalContent = getContentOfFile(pathFile);
        String newContent = originalContent.replace(oldText, textReplace);

        return newContent;
    }

    // Copiar el contenido de una cadena a un Archivo
    public void copyContentStringOfFile(String pathFile, String theContent) throws IOException {
        File targetFile = new File(pathFile);
        OutputStream out = new FileOutputStream(pathFile);

        StreamUtils.copy(theContent, StandardCharsets.UTF_8, out);
    }

    public String getContentOfFile (String originPathFile) throws IOException {
        String originFileName = originPathFile;
        InputStream contentOriginal = new FileInputStream(originFileName);
        String content = StreamUtils.copyToString(contentOriginal, StandardCharsets.UTF_8);

        return content;
    }

    // Obtener el contenido de un archivo en una cadena
    public String getStringContentFile(String originPathFile) throws IOException {
        String originFileName = originPathFile;
        InputStream contentOriginal = new FileInputStream(originFileName);
        String content = StreamUtils.copyToString(contentOriginal, StandardCharsets.UTF_8);

        return content;
    }

    public String getFileContent (String pathFile) throws IOException {
        String theFileContent = getStringFromInputStream(new FileInputStream(pathFile));
        return theFileContent;
    }

    public String getStringFromInputStream(InputStream input) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(input, writer, "UTF-8");
        return writer.toString();
    }

    public InputStream getNonClosingInputStream() throws IOException {
        InputStream in = new FileInputStream("src/test/resources/input.txt");
        return StreamUtils.nonClosing(in);
    }

}
